#!/usr/bin/env perl

use v5.38;
use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5";

use List::AllUtils 'firstval', 'lastval';
use Path::Tiny;

my @lines = grep /\S/, path('./input')->lines_utf8;
chomp @lines;

my $sum = 0;
foreach my $line (@lines) {
    my @chars = split //, $line;
    my $first_digit = firstval { /[0-9]/ } @chars;
    my $last_digit = lastval { /[0-9]/ } @chars;

    if (defined $first_digit) {
        $sum += 10 * $first_digit + $last_digit;
    }
}

say $sum;