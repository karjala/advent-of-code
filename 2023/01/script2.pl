#!/usr/bin/env perl

use v5.38;
use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5";

use List::AllUtils 'firstval', 'lastval';
use Path::Tiny;

my @lines = grep /\S/, path('./input')->lines_utf8;
chomp @lines;

my %map = (
    one   => 1,
    two   => 2,
    three => 3,
    four  => 4,
    five  => 5,
    six   => 6,
    seven => 7,
    eight => 8,
    nine  => 9,
);

my %r_map = map {( scalar(reverse($_)) , $map{$_} )} keys %map;

my $map = join '|', keys %map;
my $r_map = join '|', keys %r_map;

my $sum = 0;
foreach my $line (@lines) {
    my $first_digit;
    if ($line =~ /^[^0-9]*?($map)/) {
        $first_digit = $map{$1};
    } else {
        ($first_digit) = $line =~ /([0-9])/;
    }
    defined $first_digit or next;

    my $last_digit;
    my $r_line = reverse $line;
    if ($r_line =~ /^[^0-9]*?($r_map)/) {
        $last_digit = $r_map{$1};
    } else {
        ($last_digit) = $r_line =~ /([0-9])/;
    }
    defined $last_digit or next;

    $sum += 10 * $first_digit + $last_digit;
}

say $sum;
