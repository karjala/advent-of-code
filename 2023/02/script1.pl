#!/usr/bin/env perl

use v5.38;

my %max = (
    red   => 12,
    green => 13,
    blue  => 14,
);

my $sum = 0;

LINE:
while (<>) {
    chomp;
    my ($game_id, $sets) = /^Game (\d+)\:(.+)\z/;
    my @sets = split /[,;]/, $sets;
    foreach my $set (@sets) {
        my ($num, $color) = $set =~ /(\d+) (red|green|blue)/;
        next unless $color;
        $num <= $max{$color} or next LINE;
    }

    $sum += $game_id;
}

say $sum;