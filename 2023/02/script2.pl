#!/usr/bin/env perl

use v5.38;

use List::Util 'max';

my $sum = 0;

LINE:
while (<>) {
    s/^Game.+?\://;
    my @sets = split /[,;]/, $_;
    my %min = (red => 0, green => 0, blue => 0);
    foreach my $set (@sets) {
        my ($num, $color) = $set =~ /(\d+) (red|green|blue)/;
        next unless $color;
        $min{$color} = max($min{$color}, $num);
    }

    $sum += $min{red} * $min{green} * $min{blue};
}

say $sum;
