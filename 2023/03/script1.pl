#!/usr/bin/env perl

use v5.38;

use List::Util 'min', 'max';

my @lines = grep /\S/, <>;
chomp @lines;

my $WIDTH = length $lines[0];
my $HEIGHT = @lines;

my $sum = 0;
foreach my $y (0 .. $HEIGHT-1) {
    my @sections = $lines[$y] =~ /(\D*)(\d+)/g;
    my $length_before = 0;
    foreach my $section (@sections) {
        if ($section =~ /\d/) {
            my $x_min = max($length_before-1, 0);
            my $x_max = min($length_before + length($section), $WIDTH-1);
            my $block_length = $x_max - $x_min + 1;

            my $block = join '', map substr($_, $x_min, $block_length),
                @lines[max($y-1, 0) .. min($y+1, $HEIGHT-1)];

            if ($block =~ /[^.\d]/) {
                $sum += $section;
            }
        }
        $length_before += length $section;
    }
}

say $sum;