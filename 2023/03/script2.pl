#!/usr/bin/env perl

use v5.38;

use List::Util 'min', 'max';

my @lines = grep /\S/, <>;
chomp @lines;

my $WIDTH = length $lines[0];
my $HEIGHT = @lines;

my @stars;
my %numbers;

foreach my $y (0 .. $HEIGHT-1) {
    my @sections = $lines[$y] =~ /([^*\d]*)(\*|\d+)/g;
    my $length_before = 0;
    foreach my $section (@sections) {
        if ($section =~ /\d/) {
            $numbers{$y}{$length_before} = $section;
        } elsif ($section eq '*') {
            push @stars, [$y, $length_before];
        }
        $length_before += length $section;
    }
}

my $sum = 0;

foreach my $star (@stars) {
    my ($y, $x) = @$star;
    my @adjacent_numbers;
    my @adjacent_rows;
    push @adjacent_rows, $y-1 if $y > 0;
    push @adjacent_rows, $y+1 if $y < $HEIGHT-1;
    foreach my $row (@adjacent_rows) {
        foreach my $x_cand (keys $numbers{$row}->%*) {
            my $number = $numbers{$row}{$x_cand};
            if (max($x_cand-1, 0) <= $x <= min($x_cand + length($number), $WIDTH-1)) {
                push @adjacent_numbers, $number;
            }
        }
    }
    foreach my $x_cand (keys $numbers{$y}->%*) {
        my $number = $numbers{$y}{$x_cand};
        if ($x_cand + length($number) - 1 == $x - 1 or $x_cand == $x + 1) {
            push @adjacent_numbers, $number;
        }
    }

    if (@adjacent_numbers == 2) {
        $sum += $adjacent_numbers[0] * $adjacent_numbers[1];
    }
}

say $sum;
