#!/usr/bin/env perl

use v5.38;

use POSIX 'round';

my $score;

while (my $line = <>) {
    my ($winning, $my) = $line =~ /\:(.+?)\|(.+)$/;
    my %winning = map {($_ => 1)} $winning =~ /(\d+)/g;
    my @my = $my =~ /(\d+)/g;
    my $matches = grep $winning{$_}, @my;
    $score += round(2 ** ($matches - 1)) if $matches;
}

say $score;