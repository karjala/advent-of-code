#!/usr/bin/env perl

use v5.38;

use List::Util 'sum0';

use experimental 'builtin', 'for_list';
use builtin 'indexed';

my @cards = grep /\S/, <>;
my @num_of_cards = (1) x @cards;

foreach my ($idx, $card) (indexed @cards) {
    my ($winning, $my) = $card =~ /\:(.+?)\|(.+)$/;
    my %winning = map {($_ => 1)} $winning =~ /(\d+)/g;
    my @my = $my =~ /(\d+)/g;
    my $matches = grep $winning{$_}, @my;
    for (my $i = $idx + 1; $i <= $idx + $matches; $i++) {
        $num_of_cards[$i] += $num_of_cards[$idx];
    }
}

say sum0(@num_of_cards);
