#!/usr/bin/env perl

use v5.38;

use Data::Dumper;

my @seeds;
my @maps;

my @lines = <>;

while (my $line = shift @lines) {
    if ($line =~ /^seeds\:(.+)/) {
        @seeds = $1 =~ /(\d+)/g;
    } elsif ($line =~ /\-to\-/) {
        push @maps, [];
        while (((my $subline = shift @lines) // '') =~ /\S/) {
            push $maps[-1]->@*, [$subline =~ /(\d+)/g];
        }
    }
}

my $min_location;

SEED:
foreach my $seed (@seeds) {
    my $cur = $seed;
    MAP:
    foreach my $map (@maps) {
        LINE:
        foreach my $line (@$map) {
            if ($line->[1] <= $cur <= $line->[1] + $line->[2] - 1) {
                $cur = $line->[0] + $cur - $line->[1];
                next MAP;
            }
        }
    }

    if (! defined $min_location or $cur < $min_location) {
        $min_location = $cur;
    }
}

say $min_location;
