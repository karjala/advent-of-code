#!/usr/bin/env perl

use v5.38;

use Data::Dumper;
use List::Util 'min', 'max';

use experimental 'builtin', 'for_list';
use builtin 'indexed';

my @seed_ranges;
my @maps;

my @lines = <>;

# Read file
while (my $line = shift @lines) {
    if ($line =~ /^seeds\:(.+)/) {
        my @numbers = $1 =~ /(\d+)/g;
        while (my $start = shift @numbers) {
            my $end = $start + shift(@numbers) - 1;
            push @seed_ranges, [$start, $end];
        }
    } elsif ($line =~ /\-to\-/) {
        push @maps, [];
        while (((my $subline = shift @lines) // '') =~ /\S/) {
            push $maps[-1]->@*, [$subline =~ /(\d+)/g];
        }
    }
}

my $min_location;
sub calc_intersection ($int1, $int2) {
    my $intersection;
    if ($int1->[1] >= $int2->[0] and $int1->[0] <= $int2->[1]) {
        $intersection = [max($int1->[0], $int2->[0]), min($int1->[1], $int2->[1])];
    }
    if (! $intersection) {
        return { uncommon => [$int1], common => undef };
    } else {
        my $common = $intersection;
        my @uncommon = grep {$_->[1] >= $_->[0]}
            ([$int1->[0], $intersection->[0] - 1], [$intersection->[1] + 1, $int1->[1]]);
        return {
            uncommon => \@uncommon,
            common   => $common,
        };
    }
}

my @cursors = @seed_ranges;

MAP:
foreach my $map (@maps) {
    my @next_cursors;
    LINE:
    foreach my $line (@$map) {
        my ($dst_start, $src_start, $length) = @$line;
        RANGE:
        for (my $i = 0; $i < @cursors; $i++) {
            my $cursor = $cursors[$i] or next RANGE;
            my ($start, $end) = @$cursor;
            my $result = calc_intersection([$start, $end], [$src_start, $src_start + $length - 1]);
            my ($uncommon, $common) = $result->@{qw/ uncommon common /};
            if ($common) {
                push @next_cursors, [$common->[0] + $dst_start - $src_start, $common->[1] + $dst_start - $src_start];
                splice @cursors, $i, 1, @$uncommon;
                redo RANGE;
            }
        }
    }
    @cursors = (@cursors, @next_cursors);
}

say min(map $_->[0], @cursors);
