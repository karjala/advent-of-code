#!/usr/bin/env perl

use v5.38;

use List::Util 'product';

my @times = <> =~ /(\d+)/g;
my @distances = <> =~ /(\d+)/g;
my @winning_ways = (0) x @times;

for (my $i = 0; $i < @times; $i++) {
    my $time = $times[$i];
    my $record_distance = $distances[$i];

    # $x is how long you're pressing the button down for
    foreach (my $x = 0; $x <= $time; $x++) {
        my $distance = $x * ($time - $x);
        if ($distance > $record_distance) {
            $winning_ways[$i]++;
        }
    }
}

say product @winning_ways;
