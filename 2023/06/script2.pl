#!/usr/bin/env perl

use v5.38;

use POSIX 'ceil', 'floor';

my $t = join '', <> =~ /(\d+)/g;
my $r = join '', <> =~ /(\d+)/g;

# solve the equation for x: x * ($time - x) = record_distance
# x * time - x^2 = rec
# x^2 - tx + r = 0
# x = t +- (sqrt(t^2 - 4r) / 2)

my $x1 = $t - sqrt($t ** 2 - 4 * $r) / 2;
my $x2 = $t + sqrt($t ** 2 - 4 * $r) / 2;

say floor($x2) - ceil($x1) + 1;
