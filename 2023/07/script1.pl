#!/usr/bin/env perl

use v5.38;

use experimental 'builtin', 'for_list';
use builtin 'indexed';

my @lines = grep /\S/, <>;

my @hands;

my %card_values = reverse indexed qw/ 2 3 4 5 6 7 8 9 T J Q K A /;

foreach my $line (@lines) {
    my ($hand, $bid) = $line =~ /^(\S{5}) (\d+)/;
    $hand or die;

    push @hands, {
        hand => $hand,
        bid  => $bid,
    };

    my @cards = split //, $hand;
    my %labels;
    $labels{$_}++ foreach @cards;
    my @sorted_values = sort { $b <=> $a } values %labels;
    my $type;
    if ($sorted_values[0] == 5) {
        $type = 1;
    } elsif ($sorted_values[0] == 4) {
        $type = 2;
    } elsif ($sorted_values[0] == 3 and $sorted_values[1] == 2) {
        $type = 3;
    } elsif ($sorted_values[0] == 3) {
        $type = 4;
    } elsif ($sorted_values[0] == 2 and $sorted_values[1] == 2) {
        $type = 5;
    } elsif ($sorted_values[0] == 2) {
        $type = 6;
    } elsif ($sorted_values[0] == 1) {
        $type = 7;
    } else {
        die 'invalid type';
    }

    $hands[-1]{type} = $type;
    $hands[-1]{cards} = \@cards;
}

@hands = sort {
    $b->{type} <=> $a->{type}
        or $card_values{$a->{cards}[0]} <=> $card_values{$b->{cards}[0]}
        or $card_values{$a->{cards}[1]} <=> $card_values{$b->{cards}[1]}
        or $card_values{$a->{cards}[2]} <=> $card_values{$b->{cards}[2]}
        or $card_values{$a->{cards}[3]} <=> $card_values{$b->{cards}[3]}
        or $card_values{$a->{cards}[4]} <=> $card_values{$b->{cards}[4]}
} @hands;

my $total_winnings;
foreach my ($idx, $item) (indexed @hands) {
    my $rank = $idx + 1;
    my $winning = $rank * $item->{bid};
    $total_winnings += $winning;
}

say $total_winnings;