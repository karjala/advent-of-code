#!/usr/bin/env perl

use v5.38;

chomp(my $instructions = <>);
my @instructions = split //, $instructions;
scalar <>;
my %map;
while (<>) {
    my ($location, $left, $right) = /^(\w+) \= \((\w+), (\w+)\)$/;
    $location or die;
    $map{$location} = [$left, $right];
}

my $at = 'AAA';

my $steps = 0;
while ($at ne 'ZZZ') {
    my $instruction = $instructions[$steps % @instructions];
    $at = $map{$at}[$instruction eq 'L' ? 0 : 1];
    $steps++;
}

say $steps;
