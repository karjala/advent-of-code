#!/usr/bin/env perl

use v5.38;
use FindBin '$RealBin';
use lib "$RealBin/local/lib/perl5";

use ntheory 'lcm';

chomp(my $instructions = <>);
my @instructions = split //, $instructions;
say scalar @instructions;

scalar <>;

my %map;
while (<>) {
    my ($loc, $left, $right) = /^(\w+) = \((\w+), (\w+)\)/;
    $map{$loc} = [$left, $right];
}

my @ats = grep /A\z/, keys %map;

my @numbers;
foreach my $at (@ats) {
    my $steps = 0;
    while ($at !~ /Z\z/) {
        my $instruction = $instructions[$steps % @instructions];
        $at = $map{$at}[$instruction eq 'L' ? 0 : 1];
        $steps++;
    }
    push @numbers, $steps;
}

say lcm(@numbers);
