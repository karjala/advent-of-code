#!/usr/bin/env perl

use v5.38;

my @lines = grep /\S/, <>;
chomp @lines;

my $sum = 0;
while (my $line = shift @lines) {
    my @levels;
    $levels[0] = [$line =~ /([-\d]+)/g];
    while (grep $_ != 0, $levels[-1]->@*) {
        push @levels, [];
        for (my $i = 0; $i < $levels[-2]->@* - 1; $i++) {
            push $levels[-1]->@*, $levels[-2][$i + 1] - $levels[-2][$i];
        }
    }
    unshift $levels[-1]->@*, 0;
    foreach (my $level = $#levels - 1; $level >= 0; $level--) {
        unshift $levels[$level]->@*, $levels[$level][0] - $levels[$level+1][0];
    }
    $sum += $levels[0][0];
}

say $sum;
