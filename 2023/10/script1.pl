#!/usr/bin/env perl

use v5.38;

use Data::Dumper;
use List::Util 'any';
use POSIX 'floor';

sub belongs_to ($item, $array) {
    return any { $_ eq $item } @$array;
}

my @map = map [split //, do { chomp $_; $_ }], grep /\S/, <>;

my ($s_y, $s_x);

Y:
for ($s_y = 0; $s_y < @map; $s_y++) {
    for ($s_x = 0; $s_x < $map[$s_y]->@*; $s_x++) {
        if ($map[$s_y][$s_x] eq 'S') {
            last Y;
        }
    }
}

# dir to pipe
my %good_neighbors = (
    '12' => [ qw/- 7 J/ ],
    '10' => [ qw/- F L/ ],
    '21' => [ qw/| J L/ ],
    '01' => [ qw/| F 7/ ],
);

# pipe to dir
my %outputs;
foreach my $key (keys %good_neighbors) {
    foreach my $key2 ($good_neighbors{$key}->@*) {
        push $outputs{$key2}->@*, sprintf "%02d", 22 - $key;
    }
}

# print Dumper \%outputs;

my ($y, $x, $prev_y, $prev_x) = (undef, undef, $s_y, $s_x);
DIR:
foreach my $dir (keys %good_neighbors) {
    my ($dy, $dx) = map $_ - 1, split //, $dir;
    if (belongs_to $map[$s_y + $dy][$s_x + $dx], $good_neighbors{$dir}) {
        $y = $s_y + $dy;
        $x = $s_x + $dx;
        last DIR;
    }
}

my $steps = 1;

while ($y != $s_y or $x != $s_x) {
    my $item = $map[$y][$x];
    print Dumper $item;
    my @outputs = $outputs{$item}->@*;
    DIR:
    foreach my $dir (@outputs) {
        my ($dy, $dx) = map $_ - 1, split //, $dir;
        if ($y + $dy != $prev_y or $x + $dx != $prev_x) {
            say "dy = $dy, dx = $dx";
            $prev_y = $y;
            $prev_x = $x;
            $y = $y + $dy;
            $x = $x + $dx;
            $steps++;
            last DIR;
        }
    }
}

say $steps;
say floor($steps / 2);