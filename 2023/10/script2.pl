#!/usr/bin/env perl

use v5.38;

use Data::Dumper;
use List::Util 'any', 'all';

sub belongs_to ($item, $array) {
    return any { $_ eq $item } @$array;
}

my @map = map [split //, do { chomp $_; $_ }], grep /\S/, <>;

# calculate symbol under S
my ($s_y, $s_x);
Y:
for ($s_y = 0; $s_y < @map; $s_y++) {
    for ($s_x = 0; $s_x < $map[$s_y]->@*; $s_x++) {
        if ($map[$s_y][$s_x] eq 'S') {
            last Y;
        }
    }
}

# dir to pipe
my %good_neighbors = (
    '12' => [ qw/- 7 J/ ],
    '10' => [ qw/- F L/ ],
    '21' => [ qw/| J L/ ],
    '01' => [ qw/| F 7/ ],
);

# pipe to dir
my %outputs;
foreach my $key (keys %good_neighbors) {
    foreach my $key2 ($good_neighbors{$key}->@*) {
        push $outputs{$key2}->@*, sprintf "%02d", 22 - $key;
    }
}

my @s_dirs = grep {
    my $dir = $_;
    my ($dy, $dx) = map $_ - 1, split //, $dir;
    belongs_to $map[$s_y + $dy][$s_x + $dx], $good_neighbors{$dir};
} keys %good_neighbors;

my $s_symbol;

KEY:
foreach my $key (keys %outputs) {
    foreach my $s_dir (@s_dirs) {
        if (! belongs_to $s_dir, $outputs{$key}) {
            next KEY;
        }
    }
    $s_symbol = $key;
    last KEY;
}

$map[$s_y][$s_x] = $s_symbol;

my %contained;
my ($x, $y, $prev_x, $prev_y) = ($s_x, $s_y, -100, -100);
do {
    my $item = $map[$y][$x];
    $contained{"$y,$x"} = $item;
    my @outputs = $outputs{$item}->@*;
    OUTPUT:
    foreach my $output (@outputs) {
        my ($dy, $dx) = map $_ - 1, split //, $output;
        if ($y + $dy != $prev_y or $x + $dx != $prev_x) {
            $prev_y = $y;
            $prev_x = $x;
            $y = $y + $dy;
            $x = $x + $dx;
            last OUTPUT;
        }
    }
} until defined $contained{"$y,$x"};

my ($MAX_Y, $MAX_X) = ($#map, $#{ $map[0] });

say scalar keys %contained;

my $tiles_inside = 0;

for ($y = 0; $y <= $MAX_Y; $y++) {
    my $am_inside = 0;
    for ($x = 0; $x <= $MAX_X; $x++) {
        if (! $contained{"$y,$x"}) {
            $tiles_inside++ if $am_inside;
        } else {
            my $item = $map[$y][$x];
            if (belongs_to($item, [qw/ 7 | F /])) {
                $am_inside = 1 - $am_inside;
            }
        }
    }
}

say $tiles_inside;