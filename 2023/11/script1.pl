#!/usr/bin/env perl

use v5.38;

use Data::Dumper;
use List::Util 'all';

# map universe
my @map = map [split //, $_ =~ s/\n//r], grep /\S/, <>;

# double empty rows
for (my $y = 0; $y < @map; $y++) {
    if (all { $_ eq '.' } $map[$y]->@*) {
        splice @map, $y, 0, [$map[$y]->@*];
        $y++;
    }
}

# double empty columns
for (my $x = 0; $x < $map[0]->@*; $x++) {
    if (all { $_ eq '.' } map $_->[$x], @map) {
        splice @$_, $x, 0, '.' foreach @map;
        $x++;
    }
}

# find galaxies
my @galaxies;
foreach (my $y = 0; $y < @map; $y++) {
    foreach (my $x = 0; $x < $map[$y]->@*; $x++) {
        if ($map[$y][$x] eq '#') {
            push @galaxies, [$y, $x];
        }
    }
}

# calculate distances
my $sum_distances = 0;
for (my $i = 1; $i < @galaxies; $i++) {
    my $gal1 = $galaxies[$i];
    for (my $j = 0; $j < $i; $j++) {
        my $gal2 = $galaxies[$j];
        $sum_distances += abs($gal1->[0] - $gal2->[0]) + abs($gal1->[1] - $gal2->[1]);
    }
}

say $sum_distances;
