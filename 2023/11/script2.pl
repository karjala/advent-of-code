#!/usr/bin/env perl

use v5.38;

use Data::Dumper;
use List::Util 'all', 'min', 'max';

my $EXPANSION = 1_000_000;

# map universe
my @map = map [split //, $_ =~ s/\n//r], grep /\S/, <>;

my (@empty_rows, @empty_columns);

# mark empty rows
for (my $y = 0; $y < @map; $y++) {
    if (all { $_ eq '.' } $map[$y]->@*) {
        push @empty_rows, $y;
    }
}

# mark empty columns
for (my $x = 0; $x < $map[0]->@*; $x++) {
    if (all { $_ eq '.' } map $_->[$x], @map) {
        push @empty_columns, $x;
    }
}

# find galaxies
my @galaxies;
foreach (my $y = 0; $y < @map; $y++) {
    foreach (my $x = 0; $x < $map[$y]->@*; $x++) {
        if ($map[$y][$x] eq '#') {
            push @galaxies, [$y, $x];
        }
    }
}

# calculate distances
my $sum_distances = 0;
for (my $i = 1; $i < @galaxies; $i++) {
    my $gal1 = $galaxies[$i];
    for (my $j = 0; $j < $i; $j++) {
        my $gal2 = $galaxies[$j];

        my $num_empty_rows = grep { min($gal1->[0], $gal2->[0]) < $_ < max($gal1->[0], $gal2->[0])} @empty_rows;
        my $num_empty_cols = grep { min($gal1->[1], $gal2->[1]) < $_ < max($gal1->[1], $gal2->[1])} @empty_columns;

        $sum_distances += abs($gal1->[0] - $gal2->[0]) + abs($gal1->[1] - $gal2->[1]) + $num_empty_rows * ($EXPANSION - 1) + $num_empty_cols * ($EXPANSION - 1);
    }
}

say $sum_distances;
